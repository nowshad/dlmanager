import com.dlm.AppRunner;
import com.dlm.manager.helper.AppConf;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Created by nowshad on 7/3/16.
 */
public class DownloadTest {

    @Test
    public void testHTTP() throws IOException {
        String downloadTo = System.getProperty("user.home").concat("/Documents/");

        System.setProperty("url", "http://www.mozilla.org/media/img/home/voices/promos/download-firefox/firefox-logo.d99a8d78a049.png");
        System.setProperty("dlocation", downloadTo);

        new AppRunner().execute();
        File createdFile = new File(downloadTo + "/firefox-logo.d99a8d78a049.png");
        Assert.assertTrue("File created", createdFile.exists());

        if(createdFile.exists()){
            createdFile.delete();
        }
    }

    @Test
    public void testPauseAndResume() throws IOException {
        FileUtils.deleteDirectory(new File(AppConf.TEMP_FOLDER_LOCATION));
        String downloadTo = System.getProperty("user.home").concat("/Documents/");

        System.setProperty("url", "http://www.lua.org/images/downloadarrow.png");
        System.setProperty("dlocation", downloadTo);

        AppConf.PAUSE_AND_RESUME_DOWNLOAD = true;

        new AppRunner().execute();
        File createdFile = new File(downloadTo + "/downloadarrow.png");
        Assert.assertTrue("File creation failed", createdFile.exists());

        if(createdFile.exists()){
            createdFile.delete();
        }
    }


    @Test
    public void testDownloadFailAndResumeDownload() throws IOException {
        FileUtils.deleteDirectory(new File(AppConf.TEMP_FOLDER_LOCATION));

        AppConf.TURN_OFF_DOWNLOAD_AFTER_FIRST_BYTE = true;
        String downloadTo = System.getProperty("user.home").concat("/Documents/");

        System.setProperty("url", "http://cdn.ndtv.com/tech/alcatel_onetouch_fierce_xl.jpg");
        System.setProperty("dlocation", downloadTo);

        //app ran for first time and download exits due to interruption
        new AppRunner().execute();
        File createdFile = new File(downloadTo + "/alcatel_onetouch_fierce_xl.jpg");
        Assert.assertFalse("File shouldn't exist", createdFile.exists());
        Assert.assertEquals("partial file downloaded inside temp folder", 1024, new File(AppConf.TEMP_FOLDER_LOCATION + "alcatel_onetouch_fierce_xl.jpg").length());

        AppConf.TURN_OFF_DOWNLOAD_AFTER_FIRST_BYTE = false;
        //app ran second time to complete download
        new AppRunner().execute();
        createdFile = new File(downloadTo + "/alcatel_onetouch_fierce_xl.jpg");
        Assert.assertEquals("file downloaded full", 28389, createdFile.length()); //TODO real byte of the file here
        if(createdFile.exists()){
            createdFile.delete();
        }
    }

    @Test
    public void testSFTP() throws IOException {
        String downloadTo = System.getProperty("user.home").concat("/Documents/sftp/test/");

        System.setProperty("url", "sftp://demo.wftpserver.com/download/version.txt");
        System.setProperty("dlocation", downloadTo);
        System.setProperty("user", "demo-user");
        System.setProperty("pass", "demo-user");
        System.setProperty("port", "2222");

        new AppRunner().execute();
        File createdFile = new File(downloadTo + "/version.txt");
        Assert.assertTrue("File created", createdFile.exists());

        if(createdFile.exists()){
            createdFile.delete();
        }
    }


    @Test
    public void testFTP() throws IOException {
        String downloadTo = System.getProperty("user.home").concat("/Documents/");

        System.setProperty("url", "ftp://demo.wftpserver.com/download/16_1600x1200_bmw_x5.jpg");
        System.setProperty("dlocation", downloadTo);
        System.setProperty("user", "demo-user");
        System.setProperty("pass", "demo-user");


        new AppRunner().execute();
        File createdFile = new File(downloadTo + "/16_1600x1200_bmw_x5.jpg");
        Assert.assertTrue("File created", createdFile.exists());

       if(createdFile.exists()){
            createdFile.delete();
        }
    }
}
