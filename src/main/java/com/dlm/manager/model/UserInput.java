package com.dlm.manager.model;

/**
 * Created by nowshad on 7/3/16.
 */
public class UserInput {
    String url;
    String downloadLocation;
    String userName;
    String password;
    String port;

    public UserInput(String url, String downloadLocation, String userName, String password, String port) {
        this.url = url;
        this.downloadLocation = downloadLocation;
        this.userName = userName;
        this.password = password;
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDownloadLocation() {
        return downloadLocation;
    }

    public void setDownloadLocation(String downloadLocation) {
        this.downloadLocation = downloadLocation;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
