package com.dlm.manager;


import com.dlm.manager.helper.AppConf;
import com.dlm.manager.helper.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.vfs2.*;
import org.apache.commons.vfs2.util.RandomAccessMode;

import java.io.*;

/**
 * Created by nowshad on 7/2/16.
 */
public class FileDownloader {
    public void download(String url, String downloadTo) throws IOException {

        FileSystemOptions fsOptions = new FileSystemOptions();

        FileSystemManager fsManager = VFS.getManager();

        FileObject fo = fsManager.resolveFile(url, fsOptions);

        String fileName = fo.getContent().getFile().getName().getBaseName();

        File tempFile = new File(AppConf.TEMP_FOLDER_LOCATION + fileName);

        BufferedInputStream bis = null;
        FileOutputStream fis = null;
        try{
            InputStream inputStream = getInputStream(fo, tempFile);

            bis = new BufferedInputStream(inputStream);

            if(fileExist(fo, tempFile)){
                fis = new FileOutputStream(tempFile.getPath(), true);
            } else{
                fis = new FileOutputStream(tempFile.getPath());
            }

            byte[] buffer = new byte[1024];
            int count=0;
            while((count = bis.read(buffer,0,1024)) != -1)
            {
                fis.write(buffer, 0, count);
                //param used for unit testing
                if(AppConf.TURN_OFF_DOWNLOAD_AFTER_FIRST_BYTE){
                    return;
                }
                //param used for unit testing
                if(AppConf.PAUSE_AND_RESUME_DOWNLOAD){
                    try {
                        Thread.sleep(10 * 100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            transferFileToUserPreferredLocation(downloadTo, tempFile);
        } catch (FileSystemException | java.net.SocketException e){
            System.out.println(e.getMessage());
        } finally {
            if(fis != null){
                fis.close();
            }
            if(bis != null){
                bis.close();
            }
            fo.close();
        }
    }

    private boolean fileExist(FileObject fo, File tempFile) throws FileSystemException {
        return tempFile.exists() && tempFile.length() < fo.getContent().getSize();
    }

    private void transferFileToUserPreferredLocation(String downloadTo, File tempFile) throws IOException {
        boolean fileExist = true;
        File userLocation = null;
        int incrementer = 0;
        String fileNamePrepend = "";

        if(StringUtils.isBlank(downloadTo)){
            downloadTo = System.getProperty("user.home").concat("/Downloads/");
        }

        while(fileExist){
            userLocation = new File(downloadTo + Utils.getFileNameWithoutExtension(tempFile) + fileNamePrepend + Utils.getExtension(tempFile));
            if(userLocation.exists()){
                fileNamePrepend = "-" + (++incrementer);
            } else {
                fileExist = false;
            }
        }

        FileUtils.copyFile(tempFile, userLocation);
        System.out.println("Completed downloading. Find the file at: " + userLocation.getPath());
        tempFile.delete();
    }

    private InputStream getInputStream(FileObject fo, File file) throws IOException {
        InputStream inputStream;
        try{
            RandomAccessContent ra = fo.getContent().getRandomAccessContent(RandomAccessMode.READ);
            ra.getInputStream();
            ra.seek((fileExist(fo, file)) ? file.length() : 0);
            inputStream = ra.getInputStream();
        }catch(FileSystemException fs){
            inputStream = fo.getContent().getInputStream();
        }
        return inputStream;
    }

}
