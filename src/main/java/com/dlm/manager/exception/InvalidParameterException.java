package com.dlm.manager.exception;

/**
 * Created by nowshad on 7/3/16.
 */
public class InvalidParameterException extends Exception {
    public InvalidParameterException(String message) {
        super(message);
    }
}
