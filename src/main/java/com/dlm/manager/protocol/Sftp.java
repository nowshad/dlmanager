package com.dlm.manager.protocol;

import com.dlm.manager.helper.Notification;
import com.dlm.manager.helper.Utils;
import com.dlm.manager.helper.ValidationUtils;
import com.dlm.manager.model.UserInput;

public class Sftp extends Protocol {

    public Sftp(UserInput userInput) {
        super(userInput);
    }

    @Override
	public void validate(Notification notification){
        ValidationUtils.validateURL(userInput, notification);
		ValidationUtils.validateAuthenticationFields(userInput, notification);
        ValidationUtils.validatePort(userInput, notification);
	}

    @Override
    public String getScheme() {
        return "sftp";
    }

    @Override
    public String makeLinuxTypeUrl() {
        return Utils.makeLinuxTypeUrl(userInput);
    }


}