package com.dlm.manager.protocol;

import com.dlm.manager.model.UserInput;
import org.apache.commons.lang3.StringUtils;

public class ProtocolLocator{
    private static ProtocolLocator INSTANCE;

    private ProtocolLocator(){}

    public static ProtocolLocator getInstance(){
        if(INSTANCE == null){
            INSTANCE = new ProtocolLocator();
        }
        return INSTANCE;
    }


	public Protocol locate(UserInput userInput){
		if(StringUtils.trimToEmpty(userInput.getUrl()).startsWith("http")){
			return new Http(userInput);
		}else if(StringUtils.trimToEmpty(userInput.getUrl()).startsWith("ftp")){
			return new FTP(userInput);
		}else if(StringUtils.trimToEmpty(userInput.getUrl()).startsWith("sftp")){
			return new Sftp(userInput);
		}
        throw new UnsupportedOperationException("Protocol not implemented, contact nowshad");
	}
}