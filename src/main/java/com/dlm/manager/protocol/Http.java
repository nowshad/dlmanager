package com.dlm.manager.protocol;

import com.dlm.manager.helper.Notification;
import com.dlm.manager.helper.ValidationUtils;
import com.dlm.manager.model.UserInput;

/**
 * Created by nowshad on 7/2/16.
 */
public class Http extends Protocol {


    public Http(UserInput userInput) {
        super(userInput);
    }

    @Override
    public void validate(Notification notification) {
        ValidationUtils.validateURL(userInput, notification);
    }

    @Override
    public String getScheme() {
        return "http";
    }

    @Override
    public String makeLinuxTypeUrl() {
        return userInput.getUrl();
    }


}
