package com.dlm.manager.protocol;

import com.dlm.manager.helper.Notification;
import com.dlm.manager.helper.Utils;
import com.dlm.manager.helper.ValidationUtils;
import com.dlm.manager.model.UserInput;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by nowshad on 7/2/16.
 */
public class FTP extends Protocol {

    public FTP(UserInput userInput) {
        super(userInput);
        if(StringUtils.isBlank(userInput.getPort())){
            userInput.setPort("" + 21);
        }
    }

    @Override
	public void validate(Notification notification){
        ValidationUtils.validateURL(userInput, notification);
		ValidationUtils.validateAuthenticationFields(userInput, notification);
        ValidationUtils.validatePort(userInput, notification);
	}

    @Override
    public String getScheme() {
        return "ftp";
    }

    @Override
    public String makeLinuxTypeUrl() {
        return Utils.makeLinuxTypeUrl(userInput);
    }
}
