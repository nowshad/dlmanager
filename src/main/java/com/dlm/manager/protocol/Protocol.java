package com.dlm.manager.protocol;

import com.dlm.manager.FileDownloader;
import com.dlm.manager.helper.Notification;
import com.dlm.manager.model.UserInput;

import java.io.IOException;

public abstract class Protocol{
    public UserInput userInput;

    public Protocol(UserInput userInput){
        this.userInput = userInput;
    }

	public abstract void validate(Notification notification);

    public abstract String getScheme();

    public abstract String makeLinuxTypeUrl();

    public void download() throws IOException {
        new FileDownloader().download(makeLinuxTypeUrl(), userInput.getDownloadLocation());
    }
}