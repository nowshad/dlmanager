package com.dlm.manager.helper;

import com.dlm.manager.model.UserInput;
import org.apache.commons.lang3.StringUtils;

public class ValidationUtils{
	
	 public static void validateAuthenticationFields(UserInput userInput, Notification notification){
        if(userInput.getPassword() == null){
            notification.addError("Please pass password parameter");
        }
        if(userInput.getUserName() == null){
            notification.addError("Please pass username parameter");
        }
    }

    public static void validatePort(UserInput userInput, Notification notification){
        if(StringUtils.isBlank(userInput.getPort())){
            notification.addError("Port parameter cannot be empty");
        }
    }

    public static void validateURL(UserInput userInput, Notification notification){
        if(!Utils.isValidAppURLFormat(userInput.getUrl())){
            notification.addError("Only accepted format is http://my.file.com/file, ftp://other.file.com/other, sftp://and.also.this/ending");
        }
        if(!Utils.isValidURL(userInput.getUrl())){
            notification.addError("Not a valid URL");
        }
    }
}