package com.dlm.manager.helper;

/**
 * Created by nowshad on 7/5/16.
 */
public class AppConf {
    public static final String TEMP_FOLDER_LOCATION = System.getProperty("user.home") + "/dlmanager/temp/";
    public static boolean TURN_OFF_DOWNLOAD_AFTER_FIRST_BYTE = false;
    public static boolean PAUSE_AND_RESUME_DOWNLOAD = false;
}
