package com.dlm.manager.helper;

import com.dlm.manager.model.UserInput;
import org.apache.commons.validator.routines.UrlValidator;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nowshad on 7/3/16.
 */
public class Utils {
    public final static UrlValidator URL_VALIDATOR = new UrlValidator(UrlValidator.ALLOW_LOCAL_URLS + UrlValidator.ALLOW_ALL_SCHEMES);
    public final static Pattern URL_PATTERN = Pattern.compile("^(.*)://(.*?(?=/))/(.*)");

    public static boolean isValidURL(String url){
       return URL_VALIDATOR.isValid(url);
    }

    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i).toLowerCase();
        }
        return ext;
    }

    public static boolean isValidAppURLFormat(String url){
        if(url.contains("@")){
            return false;
        }
        return true;
    }

    public static String getFileNameWithoutExtension(File file) {
        return file.isFile() ? file.getName().split(getExtension(file))[0] : "";
    }

    public static String makeLinuxTypeUrl(UserInput userInput){
        Matcher urlMatcher = Utils.URL_PATTERN.matcher(userInput.getUrl());

        while (urlMatcher.find()) {
            return urlMatcher.group(1) + "://" + userInput.getUserName() + ":" + userInput.getPassword()
                    + "@" + urlMatcher.group(2) + ":" + userInput.getPort() + "/" + urlMatcher.group(3);
        }
        return "";
    }
}
