package com.dlm.manager.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nowshad on 7/3/16.
 */
public class Notification {
    private List<String> errors = new ArrayList<>();

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error){
        errors.add(error);
    }

    public boolean hasErrors(){
        return errors.size() > 0;
    }
}
