package com.dlm.manager.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by nowshad on 7/3/16.
 */
public class AppInitializer {
    public void init() throws IOException {
        File tempFolder = new File(AppConf.TEMP_FOLDER_LOCATION);
        if (tempFolder.exists()) {
            return;
        }
        FileUtils.forceMkdir(tempFolder);
    }
}
