package com.dlm;

import com.dlm.manager.helper.AppInitializer;
import com.dlm.manager.helper.Notification;
import com.dlm.manager.model.UserInput;
import com.dlm.manager.protocol.Protocol;
import com.dlm.manager.protocol.ProtocolLocator;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * Created by nowshad on 7/2/16.
 */
public class AppRunner {
    static {
        //turn off logging
        System.setProperty("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
    }

    public static void main(String[] args) throws IOException {
        new AppRunner().execute();
    }

    public void execute() throws IOException {
        new AppInitializer().init();

        String url = System.getProperty("url");
        String downloadLocation = System.getProperty("dlocation");
        String userName = System.getProperty("user");
        String password = System.getProperty("pass");
        String port = System.getProperty("port");

        if(StringUtils.isBlank(url)){
            System.out.println("URL cannot be blank");
            System.exit(0);
        }

        UserInput userInput = new UserInput(url, downloadLocation, userName, password, port);

        Notification notification = new Notification();

        Protocol protocol = ProtocolLocator.getInstance().locate(userInput);

        protocol.validate(notification);

        if(notification.hasErrors()){
            System.out.println("Error Found:");
            int errorCount = 0;
            for(String error : notification.getErrors()){
                System.out.println((String.valueOf(++errorCount)).concat(") ").concat(error).concat("\n"));
            }
            System.exit(0);
        }

        protocol.download();

    }
}
