##Features:
1) Default download location is user.home/downloads

2) protocol supported are http, ftp, sftp. Can be extended with ease

3) Should work in linux and windows.

4) Has resume capability.

5) Saves file in disk; so no pressure in memory.

6) Saves file in temp folder. Location is user.home/dlmanager/temp/

7) Once your download completes only then file will be moved to your desired location.
That means you destination will not piled with incomplete downloads.

8) Appropriate validation message for wrong inputs.

9) In case your download location has same name as you are downloading file, a numeric suffix will be added in the end of the file.

10) Default port for ftp is 21, can be changed if specified.

11) Tested with pause and resume

12) File name taken from the url

##Notes:
- Default format for input is:
http://my.file.com/file, ftp://other.file.com/other, sftp://and.also.this/ending etc

- http doesn't require port nor username nor password

- Error will be printed in console


###To run the app:
./gradlew run

###To run test cases:
./gradlew test -i
In test cases, file will be downloaded and removed.

###You can set 4 parameters:
1) url: url from which you want to download. Supports http, sftp, ftp.

2) dlocation: location where you want to download the file to. Default location is Downloads folder

3) user: username requires if scheme is ftp or sftp

4) pass: password

###To run the project from terminal:
./gradlew run -Durl=http://geoodk.com/images/download.png -Ddlocation=/home/nowshad/dump/
This will download the file in specified location

sftp example:
./gradlew run -Durl=sftp://demo.wftpserver.com/download/version.txt -Ddlocation=/home/nowshad/dump/ -Duser=demo-user -Dpass=demo-user -Dport=2222





